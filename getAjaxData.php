<?php
session_start(); 
include('config.php');

$foodtruckeatsOBJ   	= new foodTruckEats();
$get_unit_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('data/getUnitList');

$make_list				= '<option value=""> Select Location </option>';

if( isset($_GET['location']) AND !empty($_GET['location']) ){

	foreach ($get_unit_list as $unitID => $unitName) {
		if( isset($_GET['location']) AND $_GET['location'] == $unitName->locations[0]){
			$make_list	.= '<option value="'.$unitID.'"> '.$unitName->name.' </option>';
		}
	}

}else{

	foreach ($get_unit_list as $unitID => $unitName) {
		$make_list	.= '<option value="'.$unitID.'"> '.$unitName->name.' </option>';
	}

}

echo $make_list;

?>