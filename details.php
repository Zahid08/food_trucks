<?php
session_start(); 
include('config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";

/**
 * JSON-RPC CLient class 
 */
class JsonRpcClient {

    protected $_requestId = 1;
    protected $_contextOptions;
    protected $_url;
    
    /**
     * Constructor. Takes the connection parameters
     * 
     * @param String $url
     */
    public function __construct($url, $options = array()) {
        $headers = array('Content-type: application/json');
        if (isset($options['headers'])) {
            $headers = array_merge($headers, $options['headers']);
        }
        $this->_contextOptions = array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $headers) . "\r\n"
            )
        );
        
        $this->_url = $url;
    }
    
    /**
     * Performs a jsonRCP request and return result
     *
     * @param String $method
     * @param Array $params
     * @return mixed
     */
    public function __call($method, $params) {
        $currentId = $this->_requestId++;
        $request = array(
            'method' => $method,
            'params' => array_values($params),
            'id'     => $currentId
        );
        $request = json_encode($request);
        
        $this->_contextOptions['http']['content'] = $request;
        
        $response = file_get_contents($this->_url, false, stream_context_create($this->_contextOptions));
        $result = json_decode($response, false);
        
        if ($result->id != $currentId) {
            throw new Exception("Incorrect response id (request id: {$currentId}, response id: {$result->id})" . "\n\nResponse: " . $response);
        }
        if (isset($result->error) && $result->error) {
            throw new Exception("Request error: {$result->error->message}");
        }
        
        return $result->result;
    }
}


if( empty($_SESSION['check_existing_user']) ){

    $loginClient    = new JsonRpcClient('https://user-api.simplybook.me' . '/login/');
    $token          = $loginClient->getToken('foodtruckeats', '9b8f1d26bab26706abd2643c0209ab07a8bc5101aeb8dd1fd34272c70e9c0cdf');

    $client = new JsonRpcClient('https://user-api.simplybook.me' . '/', array(
        'headers' => array(
            'X-Company-Login: foodtruckeats',
            'X-Token: ' . $token
        )
    ));

    $loginClientA    = new JsonRpcClient('https://user-api.simplybook.me' . '/login/');
    $tokenA          = $loginClientA->getUserToken('foodtruckeats', 'admin', 'Zsexdr7586/');



    $clientA = new JsonRpcClient('https://user-api.simplybook.me' . '/admin/', array(
        'headers' => array(
            'X-Company-Login: foodtruckeats',
            'X-User-Token: ' . $tokenA
        )
    ));


    //$get_company_info       = $clientA->getCompanyInfo();
    //$get_locations_list     = $client->getLocationsList();
    //$get_unit_list          = $client->getUnitList();
    //$get_event_list         = $client->getEventList();
}else{
    //echo $_SESSION['check_existing_user']; 
}

$get_booking_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('data/getBookings');

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <title>Food Truck Eats</title>
        
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" rel="stylesheet">
        
        <link href="assets/style/style.css?time=<?php time(); ?>" rel="stylesheet">
    </head>
    <body>

        <div class="container">

            

            <div class="header" style="text-align: center;">
                <a href="<?php echo $actual_link; ?>"><img src="logo.jpg" class="main-logo"></a>
                <p style="margin-bottom:30px;"><a href="mailto:support@foodtruckeats.net" style="color:#454545;font-weight:bold;">Support@FoodTruckEats.net</a></p>
            </div>

                <p style="margin:10px 0px;text-align: center;font-size: 25px;color:#5bc0de;">
                    <?php
                        if( isset($_GET['provider']) AND !empty($_GET['provider'])  ){
                            $get_unit_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('data/getUnitList');
                            foreach ($get_unit_list as $unitID => $unitName) {
                                if( isset($_GET['provider']) AND $_GET['provider'] == $unitID){
                                    echo ''.$unitName->name;
                                }
                            }
                        }
                    ?>
                </p>
                <table class="table table-striped" style="max-width: 800px;margin: 0px auto;">

                    <tbody>

                        <tr>
                            <th width="35%">Date</th>
                            <th width="25%">Service</th>
                            <th width="25%">Truck Name</th>
                            <th>Menu</th>
                            <th>Picture</th>
                            <th>Coupon</th>
                        </tr>

                        <?php
                        $inree              = 1;
                        $inreeN             = 1;
                        $SL                 = 1;
                        $check_location     = 0;
                        $check_provider     = 0;
                        $today_date         = date('Y-m-d');
                        $show_min           = 0;


                        foreach ($get_booking_list as $bookings) {
                            $start_date     = date("Y-m-d", strtotime($bookings->start_date)); 
                            $get_booking_list_new[$start_date.' '.$inreeN]  =  $bookings; 
                            $inreeN++;
                        }

                        ksort($get_booking_list_new);

                        foreach ($get_booking_list_new as $bookings) {

                            //echo '<pre>'; print_r($bookings); echo '</pre>';

                            $start_date     = date("Y-m-d", strtotime($bookings->start_date)); 
                            
                            if( isset($_GET['location']) AND !empty($_GET['location'])  ){
                                $check_location = 1;
                            }
                            //
                            if( isset($_GET['provider']) AND !empty($_GET['provider'])  ){
                                $check_provider = 1;
                            }

                            $is_show_or_not     = 0;

                            if($check_location == 1 AND $check_provider == 1 ){

                                if($_GET['location'] == $bookings->location AND $_GET['provider'] == $bookings->unit_id){
                                   $is_show_or_not     = 1;
                                }else if($_GET['provider'] == $bookings->unit_id){
                                   $is_show_or_not     = 1;
                                }else{
                                   $is_show_or_not     = 0; 
                                }

                            }else if($check_location == 1){

                                if($_GET['location'] == $bookings->location){
                                   $is_show_or_not     = 1;
                                }else{
                                   $is_show_or_not     = 0; 
                                }

                            }else if($check_provider == 1){

                                if($_GET['provider'] == $bookings->unit_id){
                                   $is_show_or_not     = 1;
                                }else{
                                   $is_show_or_not     = 0; 
                                }

                            }else if($check_location == 0 AND $check_provider == 0 ){

                                $is_show_or_not     = 1;

                            }

                            if($is_show_or_not == 1 AND $bookings->is_confirm == 1 AND $start_date >= $today_date){
                                $getAssets=$foodtruckeatsOBJ->get_booking_details_public($bookings->id);
                            ?>
                                <tr>
                                    <td><?php echo date("l, M d Y", strtotime($bookings->start_date)); ?></td>
                                    <td><?php echo $bookings->event; ?></td>
                                    <td><?php echo $bookings->client; ?></td>
                                    <td> <a href="<?php echo !empty($getAssets['menu_url'])?$getAssets['menu_url']:'#';?>" <?php if (!empty($getAssets['menu_url'])){echo "download";} ?> class="btn <?php if (!empty($getAssets['menu_url'])){echo "btn-success";}else{echo "btn-default";} ?> btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a></td>
                                    <td>
                                        <?php
                                        if (!empty($getAssets['image_url'])){
                                            $url=$getAssets['image_url'];?>
                                            <a download="<?php echo $bookings->client.'-image';?>" href="<?php echo $url;?>" title="ImageName">
                                                <img src="<?php echo $url;?>" style="height: 41px;width: 53px;">
                                            </a>
                                            <?php
                                        }else{ ?>
                                            <?php
                                            echo '<img src="assets/media/image.jpg" style="height: 41px;width: 53px;">';
                                        }
                                        ?>
                                    </td>
                                    <td> <a href="<?php echo !empty($getAssets['coupon_url'])?$getAssets['coupon_url']:'#';?>"  <?php if (!empty($getAssets['coupon_url'])){echo "download";} ?> class="btn <?php if (!empty($getAssets['coupon_url'])){echo "btn-success";}else{echo "btn-default";} ?> btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a></td>
                                </tr>
                            <?php

                                $inree++;
                                $SL++;
                                $show_min = 1;
                            }
                        }
                        if($show_min == 0){
                            echo '<tr><th colspan="4" stlye="text-align:center;">No Records Found!</th></tr>';
                        }
                        ?>
                        
                    </tbody>

                </table>    

        </div>

        <div class="container">
            <div class="row" style="max-width: 830px;margin: 0px auto;"> 
                <hr/>
                <div class="col-md-6" style="text-align: center;margin: 10px 0px;">
                    <a href="https://foodtruckeats.net/menus" target="_blank"><button type="button" class="btn btn-danger btn-lg" style="width: 100%;">Menus</button></a>
                </div>  
                <div class="col-md-6" style="text-align: center;margin: 10px 0px;">
                    <a href="https://foodtruckeats.net/requests" target="_blank"><button type="button" class="btn btn-danger btn-lg" style="width: 100%;">Requests</button></a>
                </div>  
                <hr/>
            </div>
        </div>

        <div class="col-lg-12" style="text-align: center;padding-bottom:50px;">
            <hr/>
            <a href="<?php echo $actual_link; ?>"><button type="button" class="btn btn-info btn-sm"> &#8592; Back</button></a>
        </div>
        <!-- Javascipt -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </body>
</html>
