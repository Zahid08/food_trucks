<?php
include('../config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("layout/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include("layout/navigation.php"); ?>
    <?php include("layout/sidebar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Create New User </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content mt-30">
            <div class="row">
                <form id="data" method="post" action="functions/add_user.php" enctype="multipart/form-data" >
                <div class="col-md-12">
                    <div class="box custom-box">
                        <div class="box-header with-border">
                            <i class="fa fa-user"></i><h3 class="box-title">Create New User</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="parent-area">
                                <div class="form-area">
                                    <div class="row">
                                        <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="user_name" placeholder="sample">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" name="email" placeholder="example@gmail.com">
                                                        </div>
                                                    </div>
                                                </div>
                                               <div class="col-md-3">
                                                   <div class="form-group">
                                                       <label for="password">Password</label>
                                                       <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                                                   </div>
                                               </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="cpassword">Confirmed Password</label>
                                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirmed Password" name="cpassword">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" style="padding: 10px 69px!important;">
                            <input type="submit" class="btn btn-success" id="saveLoc">
                        </div>
                    </div>
                </div>
                </form>

                <div class="col-md-12">
                    <div class="box custom-box mt-20">
                        <div class="box-header pb-0">
                            <h3 class="box-title">Manage All Users</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="full-datatable" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $file = "../data/users.txt";
                                    $users = file_get_contents($file);
                                    $users = json_decode($users, true);
                                    function compareByID($a, $b) {
                                        return strcmp($a["id"], $b["id"]);
                                    }
                                    usort($users, 'compareByID');
                                    if ($users) {
                                    foreach($users as $key => $acity){
                                ?>
                                <tr>
                                    <td><?php echo ++$key ?></td>
                                    <td><?php echo $acity["user_name"] ?></td>
                                    <td><?php echo $acity["email"] ?></td>
                                    <td><?php echo $acity["user_type"] ?></td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-primary editData" type="button" data-id=""><i class="fa fa-edit"></i></button>
                                        <a href="" class="btn btn-xs btn-danger" type="button"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php }}?>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Location</h4>
                </div>
                <form role="form">
                    <input type="hidden" id="location_id_edit" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Location Name </label>
                            <input type="text" class="form-control" id="city_name_edit" placeholder="Location Name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="updateNow">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php include("layout/footer.php"); ?>
</div>

<script type="text/javascript">
    $("form#data").submit(function(e) {
        e.preventDefault();
        var email=$("input[name=email]").val();
        var password=$("input[name=password]").val();
        if (email=='' || password==''){
            if (email!=''){
                $("input[name=email]").css('border','1px solid green');
            } else{
                $("input[name=email]").css('border','1px solid red');
            }
            $("input[name=password]").css('border','1px solid red');
            return false;
        }else {
            $("input[name=email]").css('border','1px solid green');
            $("input[name=password]").css('border','1px solid green');
            $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    enctype: 'multipart/form-data',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data=='error'){
                            alert("This email already taken.Please Try Another");
                        }else {
                            alert("Save Successfully");
                            location.reload();
                        }
                    }
                }
            );
        }
    });
</script>
</body>
</html>
