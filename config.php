<?php
class foodTruckEats{

	public $mysqli;
    const BASE_URL = 'http://172.90.20.26/food-truck-navigation';
	function __construct(){
		$this->db_connection();
	}

	private function db_connection(){
		$db_host		=	'localhost';
		$db_user		= 	'root';
		$db_password	=	'';
		$db_name		=	'foodtruckeats';

		//$this->mysqli   = new mysqli($db_host, $db_user, $db_password, $db_name);
		//return $this->mysqli;

	}

	public function insert( $field, $data ){
		
		$query	=	"INSERT into food_truck_eats_api_data (".$field.") values ('".$data."')";

		if( $this->mysqli->query($query) === TRUE){
			echo '<p color="green">Successfully Inserted Data!</p>';
		}else{
			echo '<p color="red">Data Not Save! '.mysqli_error($this->mysqli).'</p>';
		}
	}

	public function update( $field, $data, $id ){
		
		$query	=	"Update food_truck_eats_api_data set ".$field." = '".$data."' WHERE id=".$id." ";

		if( $this->mysqli->query($query) === TRUE){
			echo '<p color="green">Data Updated Successfully!</p>';
		}else{
			echo '<p color="red">Data Not Save! '.mysqli_error($this->mysqli).'</p>';
		}
	}

	public function get_simplybook_saved_data($fileName){
		
		$getCompanyInfofileR    = fopen($fileName.".txt", "r") or die("Unable to open file!");
		$getCompanyInfofileV    = fread($getCompanyInfofileR,filesize($fileName.".txt"));

		return json_decode($getCompanyInfofileV); 
	}

	public function get_booking_details($booking_id){
	   $respones_data=$this->get_simplybook_saved_data('../data/addTrackers');
	   $return_response=[
           'menu_url'=>'',
           'image_url'=>'',
           'coupon_url'=>'',
       ];
        foreach($respones_data as $key => $value)
        {
            if($booking_id == $value->booking_id){
                $return_response=[
                    'menu_url'=>$value->menu_url,
                    'image_url'=>$value->image_url,
                    'coupon_url'=>$value->coupon_url,
                ];
                break;
            }
        }
        return $return_response;
    }

    public function get_booking_details_public($booking_id){
        $respones_data=$this->get_simplybook_saved_data('data/addTrackers');
        $return_response=[
            'menu_url'=>'',
            'image_url'=>'',
            'coupon_url'=>'',
        ];
        foreach($respones_data as $key => $value)
        {
            if($booking_id == $value->booking_id){
                $return_response=[
                    'menu_url'=>$value->menu_url,
                    'image_url'=>$value->image_url,
                    'coupon_url'=>$value->coupon_url,
                ];
                break;
            }
        }
        return $return_response;
    }


	public function get_real_ip(){

		    // Get real visitor IP behind CloudFlare network
		    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
		              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		    }
		    $client  = @$_SERVER['HTTP_CLIENT_IP'];
		    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		    $remote  = $_SERVER['REMOTE_ADDR'];

		    if(filter_var($client, FILTER_VALIDATE_IP))
		    {
		        $ip = $client;
		    }
		    elseif(filter_var($forward, FILTER_VALIDATE_IP))
		    {
		        $ip = $forward;
		    }
		    else
		    {
		        $ip = $remote;
		    }

		    return $ip;
	}


}






?>