<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview active">
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="create-trackers.php">
                    <i class="fa fa fa-laptop"></i>
                    <span>Trackers</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                 </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="change-password.php"><i class="fa fa-circle-o"></i>Change Password</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="index.php">
                    <i class="fa fa-sign-out"></i> <span>Log out</span>
                </a>
            </li>
        </ul>
    </section>
</aside>