<footer class="main-footer">
    Copyright &copy; 2019 <a target="_blank" href="https://blubirdinteractive.com/">Food Truck Eats</a>. All rights
    reserved.
</footer>

<script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="assets/js/bundle.js"></script>
<script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script src="assets/plugins/dropdown-search/select2.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
