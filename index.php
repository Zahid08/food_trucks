<?php
session_start(); 
include('config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();

if( empty($_SESSION['check_existing_user']) ){
    $_SESSION['check_existing_user'] = $foodtruckeatsOBJ->get_real_ip();
}

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";

/*$getCompanyInfofileR    = fopen("getCompanyInfo.txt", "r") or die("Unable to open file!");
$getCompanyInfofileV    = fread($getCompanyInfofileR,filesize("getCompanyInfo.txt"));

echo '<pre>'; print_r( json_decode($getCompanyInfofileV) ); 

exit();*/

/**
 * JSON-RPC CLient class 
 */
class JsonRpcClient {

    protected $_requestId = 1;
    protected $_contextOptions;
    protected $_url;
    
    /**
     * Constructor. Takes the connection parameters
     * 
     * @param String $url
     */
    public function __construct($url, $options = array()) {
        $headers = array('Content-type: application/json');
        if (isset($options['headers'])) {
            $headers = array_merge($headers, $options['headers']);
        }
        $this->_contextOptions = array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $headers) . "\r\n"
            )
        );
        
        $this->_url = $url;
    }
    
    /**
     * Performs a jsonRCP request and return result
     *
     * @param String $method
     * @param Array $params
     * @return mixed
     */
    public function __call($method, $params) {
        $currentId = $this->_requestId++;
        $request = array(
            'method' => $method,
            'params' => array_values($params),
            'id'     => $currentId
        );
        $request = json_encode($request);
        
        $this->_contextOptions['http']['content'] = $request;
        
        $response = file_get_contents($this->_url, false, stream_context_create($this->_contextOptions));
        $result = json_decode($response, false);
        
        if ($result->id != $currentId) {
            throw new Exception("Incorrect response id (request id: {$currentId}, response id: {$result->id})" . "\n\nResponse: " . $response);
        }
        if (isset($result->error) && $result->error) {
            throw new Exception("Request error: {$result->error->message}");
        }
        
        return $result->result;
    }
}


if( empty($_SESSION['check_existing_user']) ){


    $loginClient    = new JsonRpcClient('https://user-api.simplybook.me' . '/login/');
    $token          = $loginClient->getToken('foodtruckeats', '9b8f1d26bab26706abd2643c0209ab07a8bc5101aeb8dd1fd34272c70e9c0cdf');

    $client = new JsonRpcClient('https://user-api.simplybook.me' . '/', array(
        'headers' => array(
            'X-Company-Login: foodtruckeats',
            'X-Token: ' . $token
        )
    ));

    $loginClientA    = new JsonRpcClient('https://user-api.simplybook.me' . '/login/');
    $tokenA          = $loginClientA->getUserToken('foodtruckeats', 'admin', 'Zsexdr7586/');



    $clientA = new JsonRpcClient('https://user-api.simplybook.me' . '/admin/', array(
        'headers' => array(
            'X-Company-Login: foodtruckeats',
            'X-User-Token: ' . $tokenA
        )
    ));

    $get_company_info       = $clientA->getCompanyInfo();
    $get_locations_list     = $client->getLocationsList();
    $get_unit_list          = $client->getUnitList();
    $get_event_list         = $client->getEventList();

    $get_client_bookins     = array();
    $get_booking_list       = $clientA->getBookings($get_client_bookins);

    $get_company_infoJE     = json_encode($get_company_info);
    $getCompanyInfofileW    = fopen("data/getCompanyInfo.txt", "w") or die("Unable to open file!");
    fwrite($getCompanyInfofileW, $get_company_infoJE); 

    $get_locations_listJE   = json_encode($get_locations_list);
    $getCompanyInfofileW    = fopen("data/getLocationsList.txt", "w") or die("Unable to open file!");
    fwrite($getCompanyInfofileW, $get_locations_listJE);

    $get_unit_listJE        = json_encode($get_unit_list);
    $getCompanyInfofileW    = fopen("data/getUnitList.txt", "w") or die("Unable to open file!");
    fwrite($getCompanyInfofileW, $get_unit_listJE);

    $get_event_listJE       = json_encode($get_event_list);
    $getCompanyInfofileW    = fopen("data/getEventList.txt", "w") or die("Unable to open file!");
    fwrite($getCompanyInfofileW, $get_event_listJE);

    $get_booking_listJE     = json_encode($get_booking_list);
    $getCompanyInfofileW    = fopen("data/getBookings.txt", "w") or die("Unable to open file!");
    fwrite($getCompanyInfofileW, $get_booking_listJE);

    /*$foodtruckeatsOBJ->update('getCompanyInfo', $get_company_infoJE, 1);
    $foodtruckeatsOBJ->update('getLocationsList', $get_locations_listJE, 1);
    $foodtruckeatsOBJ->update('getUnitList', $get_unit_listJE, 1);
    $foodtruckeatsOBJ->update('getEventList', $get_event_listJE, 1);
    $foodtruckeatsOBJ->update('getBookings', $get_booking_listJE, 1);
    */
}

$get_locations_list     = $foodtruckeatsOBJ->get_simplybook_saved_data('data/getLocationsList');
$get_unit_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('data/getUnitList');

//echo '<pre>'; print_r( $get_unit_list ); 
//exit();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Food Truck Eats</title>

        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" rel="stylesheet">
        
        <link href="assets/style/style.css?time=<?php time(); ?>" rel="stylesheet">
    </head>
    <body>

        <div class="container">

            <div class="header" style="text-align: center;">
                <a href="<?php echo $actual_link; ?>"><img src="logo.jpg" class="main-logo"></a>
                <p style="margin-bottom:30px;"><a href="mailto:support@foodtruckeats.net" style="color:#454545;font-weight:bold;">Support@FoodTruckEats.net</a></p>
                <h2 style="color:#5bc0de;font-weight: bold;">Food Truck Calendar</h2>  

                <div id="foodtruck-search-box">
                    <form class="form-inline" action="details.php" method="get">
                      <div class="form-group">
                        <label for="email">CITY:</label>
                        <select name="location" id="location" onchange="getLocationsFunc()" class="form-control">
                            <option value="">Select City</option>
                            <?php
                                foreach ($get_locations_list as $locationID => $locationName) {
                                    if( isset($_GET['location']) AND $_GET['location'] == $locationID){
                                        echo '<option value="'.$locationID.'" selected> '.$locationName->name.' </option>';
                                    }else{
                                        echo '<option value="'.$locationID.'"> '.$locationName->name.' </option>';
                                    }
                                }
                            ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="pwd">LOCATION:</label>
                        <select name="provider" id="provider" class="form-control">
                            <option value="">Select Location</option>
                            <?php
                                foreach ($get_unit_list as $unitID => $unitName) {
                                    if( isset($_GET['provider']) AND $_GET['provider'] == $unitID){
                                        echo '<option value="'.$unitID.'" selected> '.$unitName->name.' </option>';
                                    }else{
                                        echo '<option value="'.$unitID.'"> '.$unitName->name.' </option>';
                                    }
                                }
                            ?>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-info btn-sm">Search</button>
                      <a href="<?php echo $actual_link; ?>"><button type="button" class="btn btn-info btn-sm">Reset</button></a>
                    </form> 
                </div>

            </div>
            
        </div>

<script type="text/javascript">
    function getLocationsFunc(){

        var location = $('#location').val();

        $.ajax({
            type: "POST",
            url: 'getAjaxData.php?location='+location,
            success: function(data) {

                $("#provider").html(data);

            }
        });

    }
</script>
        
        <!-- Javascipt -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </body>
</html>
