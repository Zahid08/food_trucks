<?php
class QueryJob
{

    public function arrayFilters($id, $array)
    {
      
        foreach ($array as $data) {
            if ($data["id"] == $id) {
                return $data;
                break;
            }
        }
    }

    public function getSavedData($fileName){

         $getfile = file_get_contents("admin/functions/".$fileName.".txt");
        return json_decode($getfile, true);
    }

    public function getDatafromFile($fileName){

         $getfile = file_get_contents($fileName.".txt");
        return json_decode($getfile, true);
    }

    public function getLocationDetails($locationId = ""){

        $getfile = file_get_contents("admin/functions/location.txt");

        $datas = json_decode($getfile, true);

        if($locationId != ""){
            $datas = $this->arrayFilters($locationId,  $datas);
        }
        
        return $datas;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

       return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }


}