<?php
include('../config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();
$get_locations_list     = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/getBookings');
$users     = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/users');
?>

<!DOCTYPE html>
<html>
<head>
 <?php include("layout/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include("layout/navigation.php"); ?>
    <?php include("layout/sidebar.php"); ?>

   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content mt-30">
      <div class="row">
        <div class="col-md-12">
          <div class="box custom-box">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard</h3>
            </div>
            <div class="box-body">
              <div class="parent-area">
                  <div class="col-lg-3 col-xs-6">
                      <div class="small-box bg-aqua">
                          <div class="inner">
                              <h3><?php echo count($get_locations_list);?></h3>
                              <p>Total Trackers</p>
                          </div>
                          <div class="icon">
                              <i class="ion ion-bag"></i>
                          </div>
                          <a href="create-trackers.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
    <?php include("layout/footer.php"); ?>
</div>
 
</body>
</html>
