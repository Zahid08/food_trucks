<?php
include('../config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();
$users     = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/users');
if (isset($_POST['btn'])){
    $email=$_POST['email'];
    $password=$_POST['password'];
    foreach ($users as $key => $value) {
    if($email ==$value->user_name && $password==$value->password){
        header("Location: dashboard.php");
    }else{
        echo "<script>alert('Your Username Or Password Wrong !');</script>";
        header("Location: index.php");
        die();
     }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("layout/header.php"); ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"> <b>Admin</b></a>
  </div>
  <div class="login-box-body" style="overflow: hidden;">
    <p class="login-box-msg" style="font-size: 20px;">Login to your account</p>

    <form id="data" action="#" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" value="" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" value="admin" name="password">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck">
          </div>
        </div>
        <div class="col-xs-12"  style="margin-top: 10px">
            <input type="submit" name="btn" value="Login"  class="btn btn-primary btn-block btn-flat">
        </div>
      </div>
    </form>

  </div>
</div>
<!-- jQuery 2.1.4 -->
<script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<!-- iCheck -->
<script src="assets/plugins/iCheck/icheck.min.js"></script>

<script>
    $("form#data").submit(function(e) {
        var email=$("input[name=email]").val();
        var password=$("input[name=password]").val();
        if (email=='' || password==''){
            if (email!=''){
                $("input[name=email]").css('border','1px solid green');
            } else{
                $("input[name=email]").css('border','1px solid red');
            }
            $("input[name=password]").css('border','1px solid red');
            return false;
        }else {
            return true;
        }
    });
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
