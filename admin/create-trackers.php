<?php
session_start();
include('../config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();
if( empty($_SESSION['check_existing_user']) ){
    $_SESSION['check_existing_user'] = $foodtruckeatsOBJ->get_real_ip();
}
$actual_link            = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";
$get_locations_list     = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/getLocationsList');
$get_unit_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/getUnitList');
$get_booking_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/getBookings');
$provider_name='Manage ALL Trackers';
if( isset($_GET['provider']) AND !empty($_GET['provider'])  ){
    $get_unit_list          = $foodtruckeatsOBJ->get_simplybook_saved_data('../data/getUnitList');
    foreach ($get_unit_list as $unitID => $unitName) {
        if( isset($_GET['provider']) AND $_GET['provider'] == $unitID){
            $provider_name=''.$unitName->name;
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("layout/header.php"); ?>
    <link href="../assets/style/style.css?time=<?php time(); ?>" rel="stylesheet">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include("layout/navigation.php");?>
    <?php include("layout/sidebar.php"); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Manage All Trackers</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content mt-10" style="min-height: 0px!important;">
            <div class="row">
                <div class="container">
                    <div class="header" style="text-align: center;">
                        <div id="foodtruck-search-box">
                            <form class="form-inline" action="" method="get">
                                <div class="form-group">
                                    <label for="email">CITY:</label>
                                    <select name="location" id="location" onchange="getLocationsFunc()" class="form-control">
                                        <option value="">Select City</option>
                                        <?php
                                        foreach ($get_locations_list as $locationID => $locationName) {
                                            if( isset($_GET['location']) AND $_GET['location'] == $locationID){
                                                echo '<option value="'.$locationID.'" selected> '.$locationName->name.' </option>';
                                            }else{
                                                echo '<option value="'.$locationID.'"> '.$locationName->name.' </option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">LOCATION:</label>
                                    <select name="provider" id="provider" class="form-control">
                                        <option value="">Select Location</option>
                                        <?php
                                        foreach ($get_unit_list as $unitID => $unitName) {
                                            if( isset($_GET['provider']) AND $_GET['provider'] == $unitID){
                                                echo '<option value="'.$unitID.'" selected> '.$unitName->name.' </option>';
                                            }else{
                                                echo '<option value="'.$unitID.'"> '.$unitName->name.' </option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info btn-sm">Search</button>
                                <a href="create-trackers.php"><button type="button" class="btn btn-info btn-sm">Reset</button></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="content mt-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="box custom-box mt-20">
                        <!-- /.box-header --> <div class="box-header pb-0">
                            <h3 class="box-title"><?php echo $provider_name;?></h3>
                        </div>
                        <div class="box-body">
                            <table id="full-datatable" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>Date</th>
                                    <th>Service</th>
                                    <th>Truck Name</th>
                                    <th>Menu</th>
                                    <th>Picture</th>
                                    <th>Coupon</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $inree              = 1;
                                $inreeN             = 1;
                                $SL                 = 1;
                                $check_location     = 0;
                                $check_provider     = 0;
                                $today_date         = date('Y-m-d');
                                $show_min           = 0;


                                foreach ($get_booking_list as $bookings) {
                                    $start_date     = date("Y-m-d", strtotime($bookings->start_date));
                                    $get_booking_list_new[$start_date.' '.$inreeN]  =  $bookings;
                                    $inreeN++;
                                }

                                ksort($get_booking_list_new);

                                foreach ($get_booking_list_new as $bookings) {

                                   /* echo '<pre>'; print_r($bookings); echo '</pre>';*/

                                    $start_date     = date("Y-m-d", strtotime($bookings->start_date));

                                    if( isset($_GET['location']) AND !empty($_GET['location'])  ){
                                        $check_location = 1;
                                    }
                                    //
                                    if( isset($_GET['provider']) AND !empty($_GET['provider'])  ){
                                        $check_provider = 1;
                                    }

                                    $is_show_or_not     = 0;

                                    if($check_location == 1 AND $check_provider == 1 ){

                                        if($_GET['location'] == $bookings->location AND $_GET['provider'] == $bookings->unit_id){
                                            $is_show_or_not     = 1;
                                        }else if($_GET['provider'] == $bookings->unit_id){
                                            $is_show_or_not     = 1;
                                        }else{
                                            $is_show_or_not     = 0;
                                        }

                                    }else if($check_location == 1){

                                        if($_GET['location'] == $bookings->location){
                                            $is_show_or_not     = 1;
                                        }else{
                                            $is_show_or_not     = 0;
                                        }

                                    }else if($check_provider == 1){

                                        if($_GET['provider'] == $bookings->unit_id){
                                            $is_show_or_not     = 1;
                                        }else{
                                            $is_show_or_not     = 0;
                                        }

                                    }else if($check_location == 0 AND $check_provider == 0 ){

                                        $is_show_or_not     = 1;

                                    }
                                    if($is_show_or_not == 1 AND $bookings->is_confirm == 1 AND $start_date >= $today_date){
                                        $getAssets=$foodtruckeatsOBJ->get_booking_details($bookings->id);
                                        ?>
                                        <tr>
                                            <td><?php echo $bookings->id; ?></td>
                                            <td><?php echo date("l, M d Y", strtotime($bookings->start_date)); ?></td>
                                            <td><?php echo $bookings->event; ?></td>
                                            <td><?php echo $bookings->client; ?></td>
                                            <td><a href="<?php echo !empty($getAssets['menu_url'])?$getAssets['menu_url']:'#';?>" <?php if (!empty($getAssets['menu_url'])){echo "download";} ?> class="btn <?php if (!empty($getAssets['menu_url'])){echo "btn-success";}else{echo "btn-default";} ?> btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a></td>
                                            <td>
                                                <?php
                                                if (!empty($getAssets['image_url'])){
                                                    $url=$getAssets['image_url'];?>
                                                    <a download="<?php echo $bookings->client.'-image';?>" href="<?php echo $url;?>" title="ImageName">
                                                        <img src="<?php echo $url;?>" style="height: 41px;width: 53px;">
                                                    </a>
                                                    <?php
                                                }else{
                                                    echo '<img src="../assets/media/image.jpg" style="height: 41px;width: 53px;">';
                                                }
                                                ?>
                                            </td>
                                            <td> <a href="<?php echo !empty($getAssets['coupon_url'])?$getAssets['coupon_url']:'#';?>"  <?php if (!empty($getAssets['coupon_url'])){echo "download";} ?> class="btn <?php if (!empty($getAssets['coupon_url'])){echo "btn-success";}else{echo "btn-default";} ?> btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a></td>
                                            <td class="text-center">
                                                <button class="btn btn-xs btn-primary editData" type="button" data-id="<?php echo $bookings->id ?>"><i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-modal<?php echo $bookings->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-md" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Edit [ <?php echo $bookings->client; ?> ] </h4>
                                                    </div>
                                                    <form id="data" method="post" action="functions/editTrackers.php" enctype="multipart/form-data" >
                                                        <input type="hidden" id="booking_tracker_id" value="<?php echo $bookings->id ?>" name="booking_tracker_id">
                                                        <?php  $getAssets=$foodtruckeatsOBJ->get_booking_details($bookings->id);?>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Date</label>
                                                                <input type="text" class="form-control" id="date" name="date" placeholder="date" value="<?php echo date("l, M d Y", strtotime($bookings->start_date)); ?>" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Service</label>
                                                                <input type="text" class="form-control" id="service" name="service" value="<?php echo $bookings->event; ?>" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Truck Name</label>
                                                                <input type="text" class="form-control" id="truck_name" name="truck_name"  value="<?php echo $bookings->client; ?>" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="menu">Menu</label>
                                                                <input type="file" id="menu" name="menu" value="" style="margin-bottom: 10px;">
                                                                <?php
                                                                if (!empty($getAssets['menu_url'])){
                                                                    ?>
                                                                    <a href="<?php echo !empty($getAssets['menu_url'])?$getAssets['menu_url']:'#';?>" <?php if (!empty($getAssets['menu_url'])){echo "download";} ?> class="btn-sm btn-success btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="coupon">Coupon</label>
                                                                <input type="file" id="coupon" name="coupon" style="margin-bottom: 10px;">
                                                                <?php
                                                                if (!empty($getAssets['coupon_url'])){
                                                                    ?>
                                                                    <a href="<?php echo !empty($getAssets['coupon_url'])?$getAssets['coupon_url']:'#';?>" <?php if (!empty($getAssets['coupon_url'])){echo "download";} ?> class="btn-sm btn-success btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download</a>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="picture">Picture</label>
                                                                <input type="file" id="picture" name="picture">
                                                                <p class="help-block">
                                                                    <?php
                                                                    if (!empty($getAssets['image_url'])){
                                                                        $url=$getAssets['image_url'];?>
                                                                        <a download="<?php echo $bookings->client.'-image';?>" href="<?php echo $url;?>" title="ImageName">
                                                                            <img src="<?php echo $url;?>" style="height: 66px;width: 100px;">
                                                                        </a>
                                                                        <?php
                                                                    }else{
                                                                        echo '<img src="../assets/media/image.jpg" style="height: 66px;width: 100px;">';
                                                                    }
                                                                    ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <input type="submit" class="btn btn-primary" id="updateNow">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $inree++;
                                        $SL++;
                                        $show_min = 1;
                                    }
                                }
                                if($show_min == 0){
                                    echo '<tr><th colspan="4" stlye="text-align:center;">No Records Found!</th></tr>';
                                }
                                ?>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
<?php include("layout/footer.php"); ?>
</div>
<script>
    function getLocationsFunc(){
        var location = $('#location').val();
        $.ajax({
            type: "POST",
            url: '../getAjaxData.php?location='+location,
            success: function(data) {

                $("#provider").html(data);

            }
        });
    }
    $("form#data").submit(function(e) {
        e.preventDefault();
        $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                enctype: 'multipart/form-data',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    alert("Update Successfully");
                    location.reload();
                }
            }
        );
    });
    $(".editData").on("click",function(e){
        var dataId = $(this).attr("data-id");
        $("#edit-modal"+dataId+"").modal("show");
    });
    $(function () {
        $('#full-datatable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>

</body>
</html>
