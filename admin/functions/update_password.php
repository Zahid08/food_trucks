<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/17/2019
 * Time: 6:37 PM
 */
$password = $_POST["password"];
$cpassword = $_POST["newpassword"];

$filetxt = '../../data/users.txt';
$arr_data = array();

// check if the file exists
if(file_exists($filetxt)) {
    $jsondata = file_get_contents($filetxt);
    $arr_data = json_decode($jsondata, true);
}

if($arr_data){
    foreach ($arr_data as $key => $value) {
        if($password ==$value['password']){
            $username=$value['user_name'];
            $email=$value['email'];
            $new_pass=$cpassword;
            $arr_data_new[]= array(
                "id" =>1,
                "user_name" =>$username,
                "email" =>$email,
                "password" =>$new_pass,
                "user_type" =>'admin',
            );
            $jsondata = json_encode(array_values($arr_data_new), JSON_UNESCAPED_UNICODE);
            if (file_put_contents($filetxt, $jsondata)){
             echo "success";
            }
        }else{
            echo "error";
            die();
        }
    }
}
