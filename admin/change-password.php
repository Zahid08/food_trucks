<?php
include('../config.php');
$foodtruckeatsOBJ   =   new foodTruckEats();
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("layout/header.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include("layout/navigation.php"); ?>
    <?php include("layout/sidebar.php"); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Update Password</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content mt-30">
            <div class="row">
                <form id="data" method="post" action="functions/update_password.php" enctype="multipart/form-data" >
                    <div class="col-md-12">
                        <div class="box custom-box">
                            <div class="box-header with-border">
                                <i class="fa fa-user"></i><h3 class="box-title">Update Password</h3>
                            </div>
                            <div class="box-body">
                                <div class="parent-area">
                                    <div class="form-area">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="password">Current Password</label>
                                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cpassword">New Password</label>
                                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirmed Password" name="newpassword">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer text-right" style="padding: 10px 69px!important;">
                                <input type="submit" class="btn btn-success" id="saveLoc">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <?php include("layout/footer.php"); ?>
</div>
</body>
</html>

<script>
    $("form#data").submit(function(e) {
        e.preventDefault();
        var password=$("input[name=password]").val();
        var cpassword=$("input[name=newpassword]").val();
        if (password=='' || cpassword==''){
            if (password!=''){
                $("input[name=password]").css('border','1px solid green');
            } else{
                $("input[name=newpassword]").css('border','1px solid red');
            }
            $("input[name=password]").css('border','1px solid red');
            return false;
        }else {
            $("input[name=password]").css('border','1px solid green');
            $("input[name=newpassword]").css('border','1px solid green');
            $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    enctype: 'multipart/form-data',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data=='error'){
                            alert("Your Current Password Wrong !");
                        }else {
                             alert("Your Password Changed ! Please Remember it");
                             location.reload();
                        }
                    }
                }
            );
        }
    });
</script>