<?php
session_start();
include('../../config.php');
$base_url=foodTruckEats::BASE_URL;

if (!empty($_POST['booking_tracker_id'])){

    if (!file_exists('../../assets/media/'.$_POST['booking_tracker_id'])) {
        mkdir('../../assets/media/'.$_POST['booking_tracker_id'], 0755, true);
    }

    $menu_file_url=$picture_file_url=$coupon_file_url=$NewFileName='';
    if($_FILES["menu"] !='')
    {
        $menu_file_name = strtolower($_FILES['menu']['name']);
        if ($menu_file_name){
            $cmpltPath = "../../assets/media/".$_POST['booking_tracker_id'].'/'.$menu_file_name;
            move_uploaded_file($_FILES['menu']['tmp_name'],$cmpltPath) ;
            $menu_file_url=$base_url.'/assets/media/'.$_POST['booking_tracker_id'].'/'.$menu_file_name;
        }
    }
    if($_FILES["picture"] !='')
    {
        $picture_file_name = strtolower($_FILES['picture']['name']);
        if ($picture_file_name){
            $cmpltPath = "../../assets/media/".$_POST['booking_tracker_id'].'/'.$picture_file_name;
            move_uploaded_file($_FILES['picture']['tmp_name'],$cmpltPath) ;
            $picture_file_url=$base_url.'/assets/media/'.$_POST['booking_tracker_id'].'/'.$picture_file_name;
        }
    }

    if($_FILES["coupon"] !='')
    {
        $coupon_file_name = strtolower($_FILES['coupon']['name']);
        if ($coupon_file_name){
            $cmpltPath = "../../assets/media/".$_POST['booking_tracker_id'].'/'.$coupon_file_name;
            move_uploaded_file($_FILES['coupon']['tmp_name'],$cmpltPath) ;
            $coupon_file_url=$base_url.'/assets/media/'.$_POST['booking_tracker_id'].'/'.$coupon_file_name;
        }
    }

    $formDataArray[]=[
        'booking_id'=>$_POST['booking_tracker_id'],
        'menu_url'=>$menu_file_url,
        'image_url'=>$picture_file_url,
        'coupon_url'=>$coupon_file_url,
    ];

    $filetxt = '../../data/addTrackers.txt';
    $arr_data = array();        // to store all form data
    $booking_id=$_POST['booking_tracker_id'];
    // check if the file exists
    if(file_exists($filetxt)) {
        $jsondata = file_get_contents($filetxt);
        $arr_data = json_decode($jsondata, true);
    }
    if($arr_data){
        $new_boking_id_index='';
         $found=false;
         $update_image_url=$picture_file_url;
         $update_menu_url=$menu_file_url;
         $update_coupon_url=$coupon_file_url;

       //If booking id found then update the value
        foreach ($arr_data as $key => $value) {
            if($booking_id == $value['booking_id']){
                $new_boking_id_index = $key;
                if ($value['menu_url']){
                    $update_menu_url=$value['menu_url'];
                }
                if ($value['image_url']){
                    $update_image_url=$value['image_url'];
                }
                if ($value['coupon_url']){
                    $update_coupon_url=$value['coupon_url'];
                }
                $found=true;
                break;
            }
        }
        if ($found==true){
            $arr_data[$new_boking_id_index] = array(
                'booking_id'=>$_POST['booking_tracker_id'],
                'menu_url'=>$update_menu_url,
                'image_url'=>$update_image_url,
                'coupon_url'=>$update_coupon_url,
            );
        }else{
            //array marge
            $formDataNew=[
                'booking_id'=>$_POST['booking_tracker_id'],
                'menu_url'=>$menu_file_url,
                'image_url'=>$picture_file_url,
                'coupon_url'=>$coupon_file_url,
            ];
            array_push($arr_data, $formDataNew);
        }
    }else{
         $arr_data = $formDataArray;
    }

    $jsondata = json_encode($arr_data, JSON_UNESCAPED_UNICODE);
    if(file_put_contents($filetxt, $jsondata)) echo 'Data successfully saved';

}
?>